# TSO-SW

[![Build Status](https://drone.dotya.ml/api/badges/wanderer/tso-sw/status.svg)](https://drone.dotya.ml/wanderer/tso-sw)
[![](https://img.shields.io/badge/license-WTFPL-blue)]()

this repo holds the *sawce* files for the TSO semestral work

#### hierarchy
<p align="center">
  <img alt="test_hierarchy" src="test_hierarchy.png" height="386" />
</p>

### preferred workflow

* using a container image with `robotframework`, `selenium`, `chromium` and `chromedriver` preinstalled, such as [`alpine-rf`](https://git.dotya.ml/wanderer/docker-alpine-rf) or [`archlinux-rf`](https://git.dotya.ml/wanderer/docker-archlinux-rf)

> Note: you may need to run some of the following commands as root.  You can as well learn how not to [here.](https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user)

#### get a container and run it

fetch an image from a central container repository (by default it's [DockerHub](https://hub.docker.com/)) using

```bash
docker pull immawanderer/alpine-rf:latest
```

the [`alpine-rf`](https://hub.docker.com/r/immawanderer/alpine-rf/builds) image is being rebuilt <del>on an hourly</del> on a daily basis, so I recommend updating frequently.

run the container in interactive mode, mounting user's `$PWD` into container's `/testing` folder, removing the container after exiting (so that it doesn't take up space) while preserving the work done (saved on the shared volume)  

```bash
docker run --rm -it -v $PWD:/testing immawanderer/alpine-rf:latest
```

#### run robot tests

as the `WORKDIR` of the image used in this example is set to the `/testing` folder, all that's left is to prepare a `results` directory inside there  

```bash
❄ root@alpine-rf /testing
➜ mkdir -p /testing/results
```

then simply run the test(s)

```bash
❄ root@alpine-rf /testing
➜ robot -T -d results test00.robot
```

or (when using the [`alpine-rf image`](https://git.dotya.ml/wanderer/docker-alpine-rf)) run all of the above as a designated test user `rf` (`uid=1000`) instead of `root`  

```bash
❄ root@alpine-rf /testing
➜ su rf
/testing $ mkdir -pv results
/testing $ robot -T -d results test00.robot
```

#### mermaid.js

the [diagram](test_hierarchy.png) under [hierarchy](#hierarchy) was generated using [this](./test_hierarchy.mermaid.js) [`mermaid.js`](https://mermaid-js.github.io/mermaid/#/ "mermaid.js") code
