*** Settings ***
Library  SeleniumLibrary  run_on_failure=Nothing

*** Variables ***
${URL}      https://git.dotya.ml/
${BROWSER}  chrome
${DELAY}    0.2

${BURGER_MENU}    //*[@id="navbar-expand-toggle"]
${LOGIN_BUTTON}    //*[@id="navbar"]/div[2]/a[2]
${USERNAME_FIELD}    //*[@id="user_name"]
${PASSWD_FIELD}    //*[@id="password"]
${USER_MENU_BUTTON}    //*[@id="navbar"]/div[2]/div[2]
${USER_SETTING_BUTTON}    //*[@id="menuitem_6"]
${USER_SETTINGS-ACCOUNT}    //body/div/div[2]/div[1]/a[2]
${DELETE_ACCOUNT_BUTTON}    //body/div/div[2]/div[2]/div[5]/form/div[2]/div
${CONFIRM_PASSWD_FIELD}    //*[@id="password-confirmation"]
${CONFIRM_ACCOUNT_DELETION}    //*[@id="delete-account"]/div[3]/div[2]

*** Keywords ***
Screenshot page
    Capture Page Screenshot

Input Username
    [Arguments]   ${USERNAME_FIELD}   ${USERNAME}
    Input Text    ${USERNAME_FIELD}   ${USERNAME}

Input Email
    [Arguments]   ${EMAIL_FIELD}   ${EMAIL}
    Input Text    ${EMAIL_FIELD}   ${EMAIL}

Input Password
    [Arguments]   ${PASSWD_FIELD}   ${PASSWORD}
    Input Text    ${PASSWD_FIELD}   ${PASSWORD}

Input Password_confirmation
    [Arguments]   ${CONFIRM_PASSWD_FIELD}   ${PASSWORD}
    Input Text    ${CONFIRM_PASSWD_FIELD}   ${PASSWORD}

Login page title
    Title Should Be    Sign In - dotya.ml Gitea Service

Login success
    Title Should Be    dat_test_usr - Dashboard - dotya.ml Gitea Service

Dashboard page address
    Location Should Be    https://git.dotya.ml/

Settings page address
    Location Should Be    https://git.dotya.ml/user/settings

Settings account page address
    Location Should Be    https://git.dotya.ml/user/settings/account

Homepage address
    Location Should Be    https://git.dotya.ml/

*** Test Cases ***
delete test user account (mobile)
    # based on Pixel 2 XL mobile browser declared resolution
    ${devicemetrics}=    Create Dictionary    width=${411}    height=${823}    pixelRatio=${2.0}    userAgent=Mozilla/5.0 (Linux; Mobile; Android 10) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Mobile Safari/537.36
    ${mobileemulation}=    Create Dictionary    deviceMetrics=${devicemetrics}
    ${chrome_options} =     Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    Call Method    ${chrome_options}   add_argument    headless
    Call Method    ${chrome_options}   add_argument    no-sandbox
    Call Method    ${chrome_options}   add_argument    disable-dev-shm-usage
    Call Method    ${chrome options}   add_experimental_option    mobileEmulation    ${mobileemulation}
    ${options}=     Call Method     ${chrome_options}    to_capabilities

    Open Browser    ${URL}    browser=${BROWSER}  desired_capabilities=${options}
    Set Selenium Speed  ${DELAY}
    # Screenshot page
    Log Location

    Click Element  ${BURGER_MENU}
    Screenshot page
    Click Element  ${LOGIN_BUTTON}

    Login page title
    Input Username  ${USERNAME_FIELD}  dat_test_usr
    # Input Email  ${EMAIL_FIELD}  dat_test_usr@noreply.com
    Input Password  ${PASSWD_FIELD}  D@t_p@ssw000rd987.
    Screenshot page
    Log Location
    Press Keys  None  RETURN

    Login success
    Dashboard page address
    Screenshot page
    Log Location

    Click Element  ${BURGER_MENU}
    Screenshot page
    Click Element  ${USER_MENU_BUTTON}
    Click Element  ${USER_SETTING_BUTTON}
    Screenshot page
    Log Location
    Settings page address
    Click Element  ${USER_SETTINGS-ACCOUNT}
    Settings account page address
    Screenshot page
    Log Location
    Scroll Element Into View  ${DELETE_ACCOUNT_BUTTON}
    Input Password  ${CONFIRM_PASSWD_FIELD}  D@t_p@ssw000rd987.
    Screenshot page
    Click Element  ${DELETE_ACCOUNT_BUTTON}
    Screenshot page
    Click Element  ${CONFIRM_ACCOUNT_DELETION}
    Homepage address
    Screenshot page
    Log Location

Post-conditions
    Close Browser
