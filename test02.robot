*** Settings ***
Library  SeleniumLibrary  run_on_failure=Nothing

*** Variables ***
${URL}      https://git.dotya.ml/
${BROWSER}  chrome
${DELAY}    0.2

${LOGIN_BUTTON}    //*[@id="navbar"]/div[2]/a[2]
${SIGN_IN}   //form/div[4]/button[1]

*** Keywords ***
Screenshot page
    Capture Page Screenshot

Login Should Have Failed
    Title Should Be    Sign In - dotya.ml Gitea Service

*** Test Cases ***
attempt login without creds
    ${chrome_options} =     Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    Call Method    ${chrome_options}   add_argument    headless
    Call Method    ${chrome_options}   add_argument    no-sandbox
    Call Method    ${chrome_options}   add_argument    disable-dev-shm-usage
    ${options}=     Call Method     ${chrome_options}    to_capabilities

    Open Browser    ${URL}    browser=${BROWSER}  desired_capabilities=${options}
    Set Selenium Speed  ${DELAY}
    Set Window Size    1920    1080

    Maximize Browser Window

    Click Element  ${LOGIN_BUTTON}
    Screenshot page
    Log Location

    Click Element  ${SIGN_IN}
    Screenshot page
    Login Should Have Failed
    Log Location

Post-conditions
    Close Browser
