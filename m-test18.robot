*** Settings ***
Library  SeleniumLibrary  run_on_failure=Nothing

*** Variables ***
${URL}      https://git.dotya.ml/
${BROWSER}  chrome
${DELAY}    0.2

${BURGER_MENU}    //*[@id="navbar-expand-toggle"]
${BURGER_MENU_SIGN_IN}   //*[@id="navbar"]/div[2]/a[2]
${USERNAME_FIELD}    //*[@id="user_name"]
${PASSWD_FIELD}    //*[@id="password"]
${SIGN_IN_BUTTON}   //form/div[4]/button[1]
${BURGER_AFTER_LOGIN}    //*[@id="navbar-expand-toggle"]
${BURGER_AFTER_LOGIN_EXPLORE}    //*[@id="navbar"]/a[5]
${EXPLORE_PAGE_URL}    https://git.dotya.ml/explore/repos

${ADD_SOMETHING_BUTTON}    //*[@id="navbar"]/div[2]/div[1]
${NEW_MIGRATION_BUTTON}    //*[@id="menuitem_2"]
${MIGRATION_URL_INPUT}    //*[@id="clone_addr"]
${MIGRATION_REPO_VISIBILITY_CHECKBOX}    //body/div/div[2]/div/div/form/div/div[6]/div/input
${MIGRATION_REPO_IS_A_MIRROR_CHECKBOX}    //body/div/div[2]/div/div/form/div/div[7]/div
${MIGRATION_REPO_DESCRIPTION_INPUT}    //*[@id="description"]
${MIGRATE_REPO_BUTTON}    //button[contains(., "Migrate Repository")]

${BLAKE3_CLONE_URL}    https://github.com/BLAKE3-team/BLAKE3
${BLAKE3_ABOUT_TEXT}   official implementations of the BLAKE3 cryptographic hash function
${BLAKE3_REPO_LANGUAGE_STATS}    //body/div/div[2]/div[2]/div[4]/a
${BLAKE3_REPO_URL}    https://git.dotya.ml/dat_test_usr/BLAKE3

*** Keywords ***
Screenshot page
    Capture Page Screenshot

Sign In page
    Location Should Be  https://git.dotya.ml/user/login?redirect_to=%2f

Login page title
    Title Should Be    Sign In - dotya.ml Gitea Service

Input Username
    [Arguments]   ${USERNAME_FIELD}   ${USERNAME}
    Input Text    ${USERNAME_FIELD}   ${USERNAME}

Input Password
    [Arguments]   ${PASSWD_FIELD}   ${PASSWORD}
    Input Text    ${PASSWD_FIELD}   ${PASSWORD}

Login success
    Title Should Be    dat_test_usr - Dashboard - dotya.ml Gitea Service

Dashboard page address
    Location Should Be    https://git.dotya.ml/

Input repo url
    [Arguments]   ${MIGRATION_URL_INPUT}   ${MIGRATION_URL}
    Input Text    ${MIGRATION_URL_INPUT}   ${MIGRATION_URL}

Input repo description
    [Arguments]   ${MIGRATION_REPO_DESCRIPTION_INPUT}   ${REPO_DESCRIPTION}
    Input Text    ${MIGRATION_REPO_DESCRIPTION_INPUT}   ${REPO_DESCRIPTION}

BLAKE3 repo address
    Location Should Be    ${BLAKE3_REPO_URL}


*** Test Cases ***
create a mirror of a repository (mobile)
    # based on Pixel 2 XL mobile browser declared resolution
    ${devicemetrics}=    Create Dictionary    width=${411}    height=${823}    pixelRatio=${2.0}    userAgent=Mozilla/5.0 (Linux; Mobile; Android 10) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Mobile Safari/537.36
    ${mobileemulation}=    Create Dictionary    deviceMetrics=${devicemetrics}
    ${chrome_options} =     Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    Call Method    ${chrome_options}   add_argument    headless
    Call Method    ${chrome_options}   add_argument    no-sandbox
    Call Method    ${chrome_options}   add_argument    disable-dev-shm-usage
    Call Method    ${chrome options}   add_experimental_option    mobileEmulation    ${mobileemulation}
    ${options}=     Call Method     ${chrome_options}    to_capabilities

    Open Browser    ${URL}    browser=${BROWSER}  desired_capabilities=${options}
    Set Selenium Speed  ${DELAY}
    Screenshot page
    Log Location

    Click Element  ${BURGER_MENU}
    Sleep  ${DELAY}
    Screenshot page
    Click Element  ${BURGER_MENU_SIGN_IN}
    Screenshot page
    Log Location
    Sign In page

    Login page title
    Input Username  ${USERNAME_FIELD}  dat_test_usr
    Input Password  ${PASSWD_FIELD}  D@t_p@ssw000rd987.
    Screenshot page

    Click Element  ${SIGN_IN_BUTTON}
    Screenshot page
    Log Location
    Login success
    Dashboard page address

    Click Element  ${BURGER_AFTER_LOGIN}
    Screenshot page
    Click Element  ${ADD_SOMETHING_BUTTON}
    Screenshot page
    Click Element  ${NEW_MIGRATION_BUTTON}
    Screenshot page
    Input repo url  ${MIGRATION_URL_INPUT}  ${BLAKE3_CLONE_URL}
    Screenshot page
    Unselect Checkbox  ${MIGRATION_REPO_VISIBILITY_CHECKBOX}
    Screenshot page
    Click Element  ${MIGRATION_REPO_IS_A_MIRROR_CHECKBOX}
    Screenshot page

    Scroll Element Into View  ${MIGRATE_REPO_BUTTON}
    Screenshot page
    Input repo description  ${MIGRATION_REPO_DESCRIPTION_INPUT}  ${BLAKE3_ABOUT_TEXT}
    Screenshot page
    Click Element  ${MIGRATE_REPO_BUTTON}
    Screenshot page
    Sleep  5
    Wait Until Element Is Visible  ${BLAKE3_REPO_LANGUAGE_STATS}  timeout=15.0
    Screenshot page
    Log Location
    BLAKE3 repo address

Post-conditions
    Close Browser
