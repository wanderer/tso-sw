*** Settings ***
Library  SeleniumLibrary  run_on_failure=Nothing

*** Variables ***
${URL}      https://git.dotya.ml/
${BROWSER}  chrome
${DELAY}    0.2

${BURGER_MENU}    //*[@id="navbar-expand-toggle"]
${SIGN_IN}   //form/div[4]/button[1]
${NEED_TO_REGISTER_BUTTON}    //*[@id="navbar"]/div[2]/a[1]
${USERNAME_FIELD}    //*[@id="user_name"]
${PASSWD_FIELD}    //*[@id="password"]
${EMAIL_FIELD}    //*[@id="email"]
${RETYPE_FIELD}    //*[@id="retype"]
${REGISTER_ACCOUNT_BUTTON}    //form/div/div[5]/button[1]

*** Keywords ***
Screenshot page
    Capture Page Screenshot

Input Username
    [Arguments]   ${USERNAME_FIELD}   ${USERNAME}
    Input Text    ${USERNAME_FIELD}   ${USERNAME}

Input Email
    [Arguments]   ${EMAIL_FIELD}   ${EMAIL}
    Input Text    ${EMAIL_FIELD}   ${EMAIL}

Input Password
    [Arguments]   ${PASSWD_FIELD}   ${PASSWORD}
    Input Text    ${PASSWD_FIELD}   ${PASSWORD}

Input Retype
    [Arguments]   ${RETYPE_FIELD}   ${RETYPE}
    Input Text    ${RETYPE_FIELD}   ${RETYPE}

Registration page title
    Title Should Be    Register - dotya.ml Gitea Service

*** Test Cases ***
attempt to register short password (mobile)
    # based on Pixel 2 XL mobile browser declared resolution
    ${devicemetrics}=    Create Dictionary    width=${411}    height=${823}    pixelRatio=${2.0}    userAgent=Mozilla/5.0 (Linux; Mobile; Android 10) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Mobile Safari/537.36
    ${mobileemulation}=    Create Dictionary    deviceMetrics=${devicemetrics}
    ${chrome_options} =     Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    Call Method    ${chrome_options}   add_argument    headless
    Call Method    ${chrome_options}   add_argument    no-sandbox
    Call Method    ${chrome_options}   add_argument    disable-dev-shm-usage
    Call Method    ${chrome options}   add_experimental_option    mobileEmulation    ${mobileemulation}
    ${options}=     Call Method     ${chrome_options}    to_capabilities

    Open Browser    ${URL}    browser=${BROWSER}  desired_capabilities=${options}
    Set Selenium Speed  ${DELAY}
    Screenshot page
    Log Location

    Log To Console  \n[*] Tapping on burger menu
    Click Element  ${BURGER_MENU}
    Screenshot page
    Click Element  ${NEED_TO_REGISTER_BUTTON}
    Screenshot page
    Log Location
    Log To Console  [*] Checking reg page title
    Registration page title
    Log To Console  [*] Entering bogus creds
    Input Username  ${USERNAME_FIELD}  dat_test_usr
    Input Email  ${EMAIL_FIELD}  dat_test_usr@noreply.com
    Input Password  ${PASSWD_FIELD}  password
    Input Retype  ${RETYPE_FIELD}  password
    Screenshot page

    Log To Console  [*] Attempting registration
    Click Element  ${REGISTER_ACCOUNT_BUTTON}
    Sleep  ${DELAY}
    Screenshot page
    Log Location
    Log To Console  [*] Checking that registration has failed\n
    Registration page title

Post-conditions
    Close Browser
