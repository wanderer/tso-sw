*** Settings ***
Library  SeleniumLibrary  run_on_failure=Nothing

*** Variables ***
${URL}      https://git.dotya.ml/
${BROWSER}  chrome
${DELAY}    0.2

${BURGER_MENU}    //*[@id="navbar-expand-toggle"]
${BURGER_MENU_SIGN_IN}   //*[@id="navbar"]/div[2]/a[2]
${USERNAME_FIELD}    //*[@id="user_name"]
${PASSWD_FIELD}    //*[@id="password"]
${SIGN_IN_BUTTON}   //form/div[4]/button[1]
${BURGER_AFTER_LOGIN}    //*[@id="navbar-expand-toggle"]
${BURGER_AFTER_LOGIN_EXPLORE}    //*[@id="navbar"]/a[5]
${EXPLORE_PAGE_URL}    https://git.dotya.ml/explore/repos
${SEARCH_FIELD}    //form/div/input
${SEARCH_BUTTON}    //form/div/button
${SEARCH_DOTFILES_URL}    https://git.dotya.ml/explore/repos?tab=&sort=recentupdate&q=dotfiles
${WANDERER_DOTFILES_ENTRY}    //a[@href="/wanderer/dotfiles"]
${UNSTAR_A_REPO}    //form[2]/div/button
${SHOW_STARGAZERS}    //body/div/div[2]/div[1]/div[1]/div/div[2]/form[2]/div/a
${DOTFILES_URL}    https://git.dotya.ml/wanderer/dotfiles
${DOTFILES_STARS_URL}    ${DOTFILES_URL}/stars
${STARGAZERS_LIST}    //body/div/div[2]*[contains(text(), "dat_test_usr")]

*** Keywords ***
Screenshot page
    Capture Page Screenshot

Sign In page
    Location Should Be  https://git.dotya.ml/user/login?redirect_to=%2f

Login page title
    Title Should Be    Sign In - dotya.ml Gitea Service

Input Username
    [Arguments]   ${USERNAME_FIELD}   ${USERNAME}
    Input Text    ${USERNAME_FIELD}   ${USERNAME}

Input Password
    [Arguments]   ${PASSWD_FIELD}   ${PASSWORD}
    Input Text    ${PASSWD_FIELD}   ${PASSWORD}

Login success
    Title Should Be    dat_test_usr - Dashboard - dotya.ml Gitea Service

Dashboard page address
    Location Should Be    https://git.dotya.ml/

Explore page
    Location Should Be    ${EXPLORE_PAGE_URL}

Input Search
    [Arguments]    ${SEARCH_FIELD}    ${SEARCH_TERM}
    Input Text     ${SEARCH_FIELD}    ${SEARCH_TERM}

Search dotfiles page
    Location Should Be    ${SEARCH_DOTFILES_URL}

Stargazers page
    Location Should Be    ${DOTFILES_STARS_URL}

Stargazer dotfiles address
    Location Should Contain    dotfiles/stars

Stargazer usr is present
    Should Contain    ${STARGAZERS_LIST}  dat_test_usr

*** Test Cases ***
unstar wanderer's dotfiles repo (mobile)
    # based on Pixel 2 XL mobile browser declared resolution
    ${devicemetrics}=    Create Dictionary    width=${411}    height=${823}    pixelRatio=${2.0}    userAgent=Mozilla/5.0 (Linux; Mobile; Android 10) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Mobile Safari/537.36
    ${mobileemulation}=    Create Dictionary    deviceMetrics=${devicemetrics}
    ${chrome_options} =     Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    Call Method    ${chrome_options}   add_argument    headless
    Call Method    ${chrome_options}   add_argument    no-sandbox
    Call Method    ${chrome_options}   add_argument    disable-dev-shm-usage
    Call Method    ${chrome options}   add_experimental_option    mobileEmulation    ${mobileemulation}
    ${options}=     Call Method     ${chrome_options}    to_capabilities

    Open Browser    ${URL}    browser=${BROWSER}  desired_capabilities=${options}
    Set Selenium Speed  ${DELAY}
    Screenshot page
    Log Location

    Click Element  ${BURGER_MENU}
    Sleep  ${DELAY}
    Screenshot page
    Click Element  ${BURGER_MENU_SIGN_IN}
    Screenshot page
    Log Location
    Sign In page

    Login page title
    Input Username  ${USERNAME_FIELD}  dat_test_usr
    Input Password  ${PASSWD_FIELD}  D@t_p@ssw000rd987.
    Screenshot page

    Click Element  ${SIGN_IN_BUTTON}
    Screenshot page
    Log Location
    Login success
    Dashboard page address

    Click Element  ${BURGER_AFTER_LOGIN}
    Screenshot page
    Click Element  ${BURGER_AFTER_LOGIN_EXPLORE}
    Screenshot page
    Log Location
    Explore page

    Input Search  ${SEARCH_FIELD}  dotfiles
    Screenshot page
    Log Location
    Click Element  ${SEARCH_BUTTON}
    Screenshot page
    Log Location
    Search dotfiles page

    Click Element  ${WANDERER_DOTFILES_ENTRY}
    Screenshot page
    Log Location

    Click Element  ${SHOW_STARGAZERS}
    Screenshot page
    Log Location
    Stargazers page
    Stargazer dotfiles address
    Stargazer usr is present

    Click Element  ${UNSTAR_A_REPO}
    Screenshot page

Post-conditions
    Close Browser
