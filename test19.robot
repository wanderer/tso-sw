*** Settings ***
Library  SeleniumLibrary  run_on_failure=Nothing

*** Variables ***
${URL}      https://git.dotya.ml/
${BROWSER}  chrome
${DELAY}    0.2

${LOGIN_BUTTON}    //*[@id="navbar"]/div[2]/a[2]
${SIGN_IN}   //form/div[4]/button[1]
${USERNAME_FIELD}    //*[@id="user_name"]
${PASSWD_FIELD}    //*[@id="password"]
${EXPLORE_TABBAR_BUTTON}    //*[@id="navbar"]/a[5]
${EXPLORE_PAGE_URL}    https://git.dotya.ml/explore/repos
${SEARCH_FIELD}    //form/div/input
${SEARCH_BLAKE3_URL}    https://git.dotya.ml/explore/repos?tab=&sort=recentupdate&q=BLAKE3
${DAT_TEST_USR_BLAKE3}    //a[@href="/dat_test_usr/BLAKE3"]
${BLAKE3_MIRROR_URL}    https://git.dotya.ml/dat_test_usr/BLAKE3
${REPO_SETTINGS}    //body/div/div[2]/div[1]/div[2]/div/div/a[@href="/dat_test_usr/BLAKE3/settings"]
${DELETE_REPO_BUTTON}    //button[@data-modal="#delete-repo-modal"]
${REPO_NAME_INPUT}    //body/div[2]/div/div[2]/form/div[2]/input
${BLAKE3_REPO_NAME}    BLAKE3

*** Keywords ***
Screenshot page
    Capture Page Screenshot

Input Username
    [Arguments]   ${USERNAME_FIELD}   ${USERNAME}
    Input Text    ${USERNAME_FIELD}   ${USERNAME}

Input Password
    [Arguments]   ${PASSWD_FIELD}   ${PASSWORD}
    Input Text    ${PASSWD_FIELD}   ${PASSWORD}

Login page title
    Title Should Be    Sign In - dotya.ml Gitea Service

Login success
    Title Should Be    dat_test_usr - Dashboard - dotya.ml Gitea Service

Dashboard page address
    Location Should Be    https://git.dotya.ml/

Explore page
    Location Should Be    ${EXPLORE_PAGE_URL}

Input Search
    [Arguments]    ${SEARCH_FIELD}    ${SEARCH_TERM}
    Input Text     ${SEARCH_FIELD}    ${SEARCH_TERM}

Search BLAKE3 page
    Location Should Be    ${SEARCH_BLAKE3_URL}

Input Confirmation
    [Arguments]   ${REPO_NAME_INPUT}   ${REPO_NAME}
    Input Text    ${REPO_NAME_INPUT}   ${REPO_NAME}

*** Test Cases ***
delete migrated repo
    ${chrome_options} =     Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    Call Method    ${chrome_options}   add_argument    headless
    Call Method    ${chrome_options}   add_argument    no-sandbox
    Call Method    ${chrome_options}   add_argument    disable-dev-shm-usage
    ${options}=     Call Method     ${chrome_options}    to_capabilities

    Open Browser    ${URL}    browser=${BROWSER}  desired_capabilities=${options}
    Set Selenium Speed  ${DELAY}

    Set Window Size    1920    1080
    Maximize Browser Window

    Wait Until Element Is Visible  ${LOGIN_BUTTON}  timeout=10.0
    Click Element  ${LOGIN_BUTTON}
    Sleep  ${DELAY}
    Screenshot page
    Log Location
    Login page title
    Input Username  ${USERNAME_FIELD}  dat_test_usr
    Input Password  ${PASSWD_FIELD}  D@t_p@ssw000rd987.
    Screenshot page
    Press Keys  None  RETURN
    Sleep  ${DELAY}
    Screenshot page
    Log Location
    Login success
    Dashboard page address
    Click Element  ${EXPLORE_TABBAR_BUTTON}
    Sleep  ${DELAY}
    Screenshot page
    Log Location
    Explore page
    Input Search  ${SEARCH_FIELD}  ${BLAKE3_REPO_NAME}
    Screenshot page

    Press Keys  None  RETURN
    Sleep  ${DELAY}
    Screenshot page
    Log Location
    Search BLAKE3 page

    Click Element  ${DAT_TEST_USR_BLAKE3}
    Sleep  ${DELAY}
    Screenshot page
    Log Location

    Click Element  ${REPO_SETTINGS}
    Sleep  ${DELAY}
    Screenshot page
    Log Location
    Scroll Element Into View  ${DELETE_REPO_BUTTON}
    Screenshot page
    Click Element  ${DELETE_REPO_BUTTON}
    Sleep  ${DELAY}
    Screenshot page
    Set Focus To Element  ${REPO_NAME_INPUT}
    Input Confirmation  ${REPO_NAME_INPUT}  ${BLAKE3_REPO_NAME}
    Screenshot page
    Press Keys  None  RETURN
    Handle Alert
    Screenshot page
    Log Location
    Dashboard page address

Post-conditions
    Log Location
    Close Browser
