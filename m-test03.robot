*** Settings ***
Library  SeleniumLibrary  run_on_failure=Nothing

*** Variables ***
${URL}      https://git.dotya.ml/
${BROWSER}  chrome
${DELAY}    0.2

${BURGER_MENU}    //*[@id="navbar-expand-toggle"]
${EXPLORE_BUTTON}   //*[@id="navbar"]/a[2]
${SORT_BUTTON}  //*[@id="menutext_1"]/i
${MOST_STARS}  //*[@id="menuitem_7"]

*** Keywords ***
Screenshot page
    Capture Page Screenshot

Most stars sorted
    Location Should Be  https://git.dotya.ml/explore/repos?sort=moststars&q=&tab=

*** Test Cases ***
sort repos by "Most stars" (mobile)
    # based on Pixel 2 XL mobile browser declared resolution
    ${devicemetrics}=    Create Dictionary    width=${411}    height=${823}    pixelRatio=${2.0}    userAgent=Mozilla/5.0 (Linux; Mobile; Android 10) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Mobile Safari/537.36
    ${mobileemulation}=    Create Dictionary    deviceMetrics=${devicemetrics}
    ${chrome_options} =     Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    Call Method    ${chrome_options}   add_argument    headless
    Call Method    ${chrome_options}   add_argument    no-sandbox
    Call Method    ${chrome_options}   add_argument    disable-dev-shm-usage
    Call Method    ${chrome options}   add_experimental_option    mobileEmulation    ${mobileemulation}
    ${options}=     Call Method     ${chrome_options}    to_capabilities

    Open Browser    ${URL}    browser=${BROWSER}  desired_capabilities=${options}
    Set Selenium Speed  ${DELAY}
    Screenshot page
    Log Location

    Click Element  ${BURGER_MENU}
    Screenshot page

    Click Element  ${EXPLORE_BUTTON}
    Screenshot page
    Log Location

    Click Element  ${SORT_BUTTON}
    Sleep  ${DELAY}
    Screenshot page
    Log Location

    Click Element  ${MOST_STARS}
    Most stars sorted
    Log Location
    Sleep  ${DELAY}
    Screenshot page
    Click Element  ${SORT_BUTTON}
    Screenshot page

Post-conditions
    Close Browser
