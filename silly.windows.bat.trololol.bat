REM test-set-desktop
FOR /L %%A IN (1,1,9) DO (
  robot -o "TC_0%%A - Output" -r "TC_0%%A - Report" -l "TC_0%%A - Log" "test0%%A.robot"
)
FOR /L %%A IN (10,1,19) DO (
  robot -o "TC_%%A - Output" -r "TC_%%A - Report" -l "TC_%%A - Log" "test%%A.robot"
)
robot -o "TC_00 - Output" -r "TC_00 - Report" -l "TC_00 - Log" "test00.robot"

REM test-set-mobile
FOR /L %%A IN (1,1,9) DO (
  robot -o "TC_m_0%%A - Output" -r "TC_m_0%%A - Report" -l "TC_m_0%%A - Log" "m-test0%%A.robot"
)
FOR /L %%A IN (10,1,19) DO (
  robot -o "TC_m_%%A - Output" -r "TC_m_%%A - Report" -l "TC_m_%%A - Log" "m-test%%A.robot"
)
robot -o "TC_m_00 - Output" -r "TC_m_00 - Report" -l "TC_m_00 - Log" "m-test00.robot"
