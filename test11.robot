*** Settings ***
Library  SeleniumLibrary  run_on_failure=Nothing

*** Variables ***
${URL}      https://git.dotya.ml/
${BROWSER}  chrome
${DELAY}    0.2

${LOGIN_BUTTON}    //*[@id="navbar"]/div[2]/a[2]
${SIGN_IN}   //form/div[4]/button[1]
${USERNAME_FIELD}    //*[@id="user_name"]
${PASSWD_FIELD}    //*[@id="password"]

*** Keywords ***
Screenshot page
    Capture Page Screenshot

Input Email
    [Arguments]   ${USERNAME_FIELD}   ${EMAIL}
    Input Text    ${USERNAME_FIELD}   ${EMAIL}

Input Password
    [Arguments]   ${PASSWD_FIELD}   ${PASSWORD}
    Input Text    ${PASSWD_FIELD}   ${PASSWORD}

Login page title
    Title Should Be    Sign In - dotya.ml Gitea Service

Login success
    Title Should Be    dat_test_usr - Dashboard - dotya.ml Gitea Service

Dashboard page address
    Location Should Be    https://git.dotya.ml/

Homepage address
    Location Should Be    https://git.dotya.ml


*** Test Cases ***
log in to an account using email
    ${chrome_options} =     Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    Call Method    ${chrome_options}   add_argument    headless
    Call Method    ${chrome_options}   add_argument    no-sandbox
    Call Method    ${chrome_options}   add_argument    disable-dev-shm-usage
    ${options}=     Call Method     ${chrome_options}    to_capabilities

    Open Browser    ${URL}    browser=${BROWSER}  desired_capabilities=${options}
    Set Selenium Speed  ${DELAY}

    Set Window Size    1920    1080
    Maximize Browser Window

    Click Element  ${LOGIN_BUTTON}
    Sleep  ${DELAY}
    Screenshot page
    Log Location
    Login page title
    Input Email  ${USERNAME_FIELD}  dat_test_usr@noreply.com
    Input Password  ${PASSWD_FIELD}  D@t_p@ssw000rd987.
    Screenshot page
    Press Keys  None  RETURN

    Screenshot page
    Log Location
    Login success
    Dashboard page address

Post-conditions
    Log Location
    Close Browser
