#!/bin/bash

echo "[*] hello from robot tests runner (haha)"
# reveal env (program versions)
echo "[i] bash at: $(which bash)"
bash --version | head -n 1
echo "[i] python at: $(which python)"
python --version
echo "[i] robot at: $(which robot)" || (echo "[x] robot is not on path ($PATH)\n[*] exiting..." && exit 1)
robot --version || true
if [[ "$OSTYPE" != "darwin"* ]]; then
	# not quite sure of the paths of these under macOS
	# even under GNU/Linux the package name varies
	chrome --version || chrome-browser --version || chromium --version || chromium-browser --version
	echo "[i] chromedriver at: $(which chromedriver)"
	chromedriver --version

# prepare a results directory
echo "[*] creating a \"results\" directory structure"

for i in {00..09}; do
	mkdir -pv results/test-set-desktop/test$i
	mkdir -pv results/test-set-mobile/m-test$i
done

echo "[*] running automated tests..."
# test-set-desktop
for i in {01..09}; do
	robot -T -d results/test-set-desktop/test$i test$i.robot
done
for i in {10..19}; do
	robot -T -d results/test-set-desktop/test$i test$i.robot
done
robot -T -d results/test-set-desktop/test00 test00.robot

# test-set-mobile
for i in {01..09}; do
	robot -T -d results/test-set-mobile/m-test$i m-test$i.robot
done
for i in {10..19}; do
	robot -T -d results/test-set-mobile/m-test$i m-test$i.robot
done
robot -T -d results/test-set-mobile/m-test00 m-test00.robot

echo "[*] seems like the final test has run"
echo "[*] test results can be found at $PWD/results"
echo "[*] bye..."
