*** Settings ***
Library  SeleniumLibrary  run_on_failure=Nothing

*** Variables ***
${URL}      https://git.dotya.ml/
${BROWSER}  chrome
${DELAY}    0.2

${LOGIN_BUTTON}    //*[@id="navbar"]/div[2]/a[2]
${SIGN_IN}   //form/div[4]/button[1]
${USERNAME_FIELD}    //*[@id="user_name"]
${PASSWD_FIELD}    //*[@id="password"]
${EXPLORE_TABBAR_BUTTON}    //*[@id="navbar"]/a[5]
${EXPLORE_PAGE_URL}    https://git.dotya.ml/explore/repos
${SEARCH_FIELD}    //form/div/input
${SEARCH_DOTFILES_URL}    https://git.dotya.ml/explore/repos?tab=&sort=recentupdate&q=dotfiles
${WANDERER_DOTFILES_ENTRY}    //a[@href="/wanderer/dotfiles"]
${UNSTAR_A_REPO}    //form[2]/div/button
${SHOW_STARGAZERS}    //body/div/div[2]/div[1]/div[1]/div/div[2]/form[2]/div/a
${DOTFILES_URL}    https://git.dotya.ml/wanderer/dotfiles
${DOTFILES_STARS_URL}    ${DOTFILES_URL}/stars
${STARGAZERS_LIST}    //body/div/div[2]/div[2]/ul/li/*[contains(text(), "dat_test_usr")]

*** Keywords ***
Screenshot page
    Capture Page Screenshot

Input Username
    [Arguments]   ${USERNAME_FIELD}   ${USERNAME}
    Input Text    ${USERNAME_FIELD}   ${USERNAME}

Input Password
    [Arguments]   ${PASSWD_FIELD}   ${PASSWORD}
    Input Text    ${PASSWD_FIELD}   ${PASSWORD}

Login page title
    Title Should Be    Sign In - dotya.ml Gitea Service

Login success
    Title Should Be    dat_test_usr - Dashboard - dotya.ml Gitea Service

Dashboard page address
    Location Should Be    https://git.dotya.ml/

Explore page
    Location Should Be    ${EXPLORE_PAGE_URL}

Input Search
    [Arguments]    ${SEARCH_FIELD}    ${SEARCH_TERM}
    Input Text     ${SEARCH_FIELD}    ${SEARCH_TERM}

Search dotfiles page
    Location Should Be    ${SEARCH_DOTFILES_URL}

Stargazers page
    Location Should Be    ${DOTFILES_STARS_URL}

Stargazer dotfiles address
    Location Should Contain    dotfiles/stars

Stargazer usr is present
    Should Contain    ${STARGAZERS_LIST}  dat_test_usr


*** Test Cases ***
unstar wanderer's dotfiles repo
    ${chrome_options} =     Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    Call Method    ${chrome_options}   add_argument    headless
    Call Method    ${chrome_options}   add_argument    no-sandbox
    Call Method    ${chrome_options}   add_argument    disable-dev-shm-usage
    ${options}=     Call Method     ${chrome_options}    to_capabilities

    Open Browser    ${URL}    browser=${BROWSER}  desired_capabilities=${options}
    Set Selenium Speed  ${DELAY}

    Set Window Size    1920    1080
    Maximize Browser Window

    Wait Until Element Is Visible  ${LOGIN_BUTTON}  timeout=10.0
    Click Element  ${LOGIN_BUTTON}
    Sleep  ${DELAY}
    Screenshot page
    Log Location
    Login page title
    Input Username  ${USERNAME_FIELD}  dat_test_usr
    Input Password  ${PASSWD_FIELD}  D@t_p@ssw000rd987.
    Screenshot page
    Press Keys  None  RETURN
    Sleep  ${DELAY}
    Screenshot page
    Log Location
    Login success
    Dashboard page address
    Click Element  ${EXPLORE_TABBAR_BUTTON}
    Sleep  ${DELAY}
    Screenshot page
    Log Location
    Explore page
    Input Search  ${SEARCH_FIELD}  dotfiles
    Screenshot page

    Press Keys  None  RETURN
    Sleep  ${DELAY}
    Screenshot page
    Log Location
    Search dotfiles page

    Click Element  ${WANDERER_DOTFILES_ENTRY}
    Sleep  ${DELAY}
    Screenshot page
    Log Location

    Click Element  ${SHOW_STARGAZERS}
    Sleep  ${DELAY}
    Screenshot page
    Log Location
    Stargazers page
    Stargazer dotfiles address
    Stargazer usr is present

    Click Element  ${UNSTAR_A_REPO}
    Screenshot page

Post-conditions
    Log Location
    Close Browser
