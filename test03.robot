*** Settings ***
Library  SeleniumLibrary  run_on_failure=Nothing

*** Variables ***
${URL}      https://git.dotya.ml/
${BROWSER}  chrome
${DELAY}    0.2

${EXPLORE_BUTTON}   //*[@id="navbar"]/a[2]
${SORT}  //*[@id="menutext_1"]
${MOST_STARS}  //*[@id="menuitem_7"]

*** Keywords ***
Screenshot page
    Capture Page Screenshot

*** Test Cases ***
open page, click Explore button and sort by "Most stars"
    ${chrome_options} =     Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    Call Method    ${chrome_options}   add_argument    headless
    Call Method    ${chrome_options}   add_argument    no-sandbox
    Call Method    ${chrome_options}   add_argument    disable-dev-shm-usage
    ${options}=     Call Method     ${chrome_options}    to_capabilities

    Open Browser    ${URL}    browser=${BROWSER}  desired_capabilities=${options}
    Set Selenium Speed  ${DELAY}
    Set Window Size    1920    1080
    Screenshot page

    Maximize Browser Window
    Click Element  ${EXPLORE_BUTTON}
    Sleep  ${DELAY}
    Screenshot page
    Log Location

    Click Element  ${SORT}
    Sleep  ${DELAY}
    Screenshot page
    Log Location

    Click Element  ${MOST_STARS}
    Sleep  ${DELAY}
    Screenshot page
    Log Location

Post-conditions
    Close Browser
