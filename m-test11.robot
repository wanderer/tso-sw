*** Settings ***
Library  SeleniumLibrary  run_on_failure=Nothing

*** Variables ***
${URL}      https://git.dotya.ml/
${BROWSER}  chrome
${DELAY}    0.2

${BURGER_MENU}    //*[@id="navbar-expand-toggle"]
${BURGER_MENU_SIGN_IN}   //*[@id="navbar"]/div[2]/a[2]
${USERNAME_FIELD}    //*[@id="user_name"]
${PASSWD_FIELD}    //*[@id="password"]
${SIGN_IN_BUTTON}   //form/div[4]/button[1]

*** Keywords ***
Screenshot page
    Capture Page Screenshot

Sign In page
    Location Should Be  https://git.dotya.ml/user/login?redirect_to=%2f

Login page title
    Title Should Be    Sign In - dotya.ml Gitea Service

Input Email
    [Arguments]   ${USERNAME_FIELD}   ${EMAIL}
    Input Text    ${USERNAME_FIELD}   ${EMAIL}

Input Password
    [Arguments]   ${PASSWD_FIELD}   ${PASSWORD}
    Input Text    ${PASSWD_FIELD}   ${PASSWORD}

Login success
    Title Should Be    dat_test_usr - Dashboard - dotya.ml Gitea Service

Dashboard page address
    Location Should Be    https://git.dotya.ml/

*** Test Cases ***
log in to an account using email (mobile)
    # based on Pixel 2 XL mobile browser declared resolution
    ${devicemetrics}=    Create Dictionary    width=${411}    height=${823}    pixelRatio=${2.0}    userAgent=Mozilla/5.0 (Linux; Mobile; Android 10) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Mobile Safari/537.36
    ${mobileemulation}=    Create Dictionary    deviceMetrics=${devicemetrics}
    ${chrome_options} =     Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    Call Method    ${chrome_options}   add_argument    headless
    Call Method    ${chrome_options}   add_argument    no-sandbox
    Call Method    ${chrome_options}   add_argument    disable-dev-shm-usage
    Call Method    ${chrome options}   add_experimental_option    mobileEmulation    ${mobileemulation}
    ${options}=     Call Method     ${chrome_options}    to_capabilities

    Open Browser    ${URL}    browser=${BROWSER}  desired_capabilities=${options}
    Set Selenium Speed  ${DELAY}
    Screenshot page
    Log Location

    Click Element  ${BURGER_MENU}
    Sleep  ${DELAY}
    Screenshot page
    Click Element  ${BURGER_MENU_SIGN_IN}
    Screenshot page
    Log Location
    Sign In page

    Login page title
    Input Email  ${USERNAME_FIELD}  dat_test_usr@noreply.com
    Input Password  ${PASSWD_FIELD}  D@t_p@ssw000rd987.
    Screenshot page

    Click Element  ${SIGN_IN_BUTTON}
    Screenshot page
    Log Location
    Login success
    Dashboard page address

Post-conditions
    Close Browser
