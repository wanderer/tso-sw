graph TD
  A -->|test set| B(fa:fa-cog automated)
  A[rf tests] -->|test set| C(fa:fa-wrench manual)

  B --> |test set|D(fa:fa-desktop desktop)
  B --> |test set|E(fa:fa-mobile mobile)
  C --> |test|F(test00-19)

  D --> |test|G(test00-19)
  E --> |test|H(m-test00-19)
