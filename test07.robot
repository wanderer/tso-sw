*** Settings ***
Library  SeleniumLibrary  run_on_failure=Nothing

*** Variables ***
${URL}      https://git.dotya.ml/
${BROWSER}  chrome
${DELAY}    0.2

${LOGIN_BUTTON}    //*[@id="navbar"]/div[2]/a[2]
${SIGN_IN}   //form/div[4]/button[1]
${NEED_TO_REGISTER_BUTTON}    //*[@id="navbar"]/div[2]/a[1]
${USERNAME_FIELD}    //*[@id="user_name"]
${PASSWD_FIELD}    //*[@id="password"]
${EMAIL_FIELD}    //*[@id="email"]
${RETYPE_FIELD}    //*[@id="retype"]
${REGISTER_ACCOUNT_BUTTON}    //form/div/div[5]/button[1]

*** Keywords ***
Screenshot page
    Capture Page Screenshot

Input Username
    [Arguments]   ${USERNAME_FIELD}   ${USERNAME}
    Input Text    ${USERNAME_FIELD}   ${USERNAME}

Input Email
    [Arguments]   ${EMAIL_FIELD}   ${EMAIL}
    Input Text    ${EMAIL_FIELD}   ${EMAIL}

Input Password
    [Arguments]   ${PASSWD_FIELD}   ${PASSWORD}
    Input Text    ${PASSWD_FIELD}   ${PASSWORD}

Input Retype
    [Arguments]   ${RETYPE_FIELD}   ${RETYPE}
    Input Text    ${RETYPE_FIELD}   ${RETYPE}

Registration page title
    Title Should Be    Register - dotya.ml Gitea Service

*** Test Cases ***
attempt to register short password
    ${chrome_options} =     Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    Call Method    ${chrome_options}   add_argument    headless
    Call Method    ${chrome_options}   add_argument    no-sandbox
    Call Method    ${chrome_options}   add_argument    disable-dev-shm-usage
    ${options}=     Call Method     ${chrome_options}    to_capabilities

    Open Browser    ${URL}    browser=${BROWSER}  desired_capabilities=${options}
    Set Selenium Speed  ${DELAY}
    Set Window Size    1920    1080
    Maximize Browser Window

    Log To Console  \n[*] Clicking on 'Login' button
    Click Element  ${LOGIN_BUTTON}
    Sleep  ${DELAY}
    Screenshot page
    Log Location
    Log To Console  [*] Clicking on 'Need to register'
    Click Element  ${NEED_TO_REGISTER_BUTTON}
    Screenshot page
    Log Location
    Log To Console  [*] Checking reg page title
    Registration page title
    Log To Console  [*] Entering bogus creds
    Input Username  ${USERNAME_FIELD}  dat_test_username
    Input Email  ${EMAIL_FIELD}  dat_ultratest_email@mt2015.com
    Input Password  ${PASSWD_FIELD}  password
    Input Retype  ${RETYPE_FIELD}  password
    Screenshot page

    Log To Console  [*] Attempting registration
    Click Element  ${REGISTER_ACCOUNT_BUTTON}
    Sleep  ${DELAY}
    Screenshot page
    Log Location
    Log To Console  [*] Checking that registration has failed\n
    Registration page title

Post-conditions
    Close Browser
